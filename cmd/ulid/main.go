package main

import (
	crand "crypto/rand"
	"io"
	"math/rand"

	"codeberg.org/gruf/go-fflag"
	"codeberg.org/gruf/go-fsys"
	"codeberg.org/gruf/go-ulid"
	"golang.org/x/sys/unix"
)

var (
	stderr = fsys.Stderr()
	stdout = fsys.Stdout()
)

func main() {
	var code int

	// Run main application
	if err := run(); err != nil {
		stderr.WriteString("FATAL: " + err.Error() + "\n")
		code = 1
	}

	// Exit with code
	unix.Exit(code)
}

func run() error {
	var (
		tstr string
		fast bool

		entropy io.Reader
	)

	// Set and parse runtime CLI flags
	fflag.StringVar(&tstr, "t", "time", "", "Use given time for ULID timestamp (format: RFC1123)")
	fflag.BoolVar(&fast, "f", "fast", false, "Use fast (unsafe) rand source")
	fflag.Func("h", "help", "Print usage information", usage)

	// Parse runtime flags
	_, err := fflag.Parse()
	if err != nil {
		return err
	}

	// Load current ts
	now := ulid.Now()

	switch {
	case fast:
		// Use faster math-rand source
		src := rand.NewSource(int64(now))
		entropy = rand.New(src)

	default:
		// Use safe rand source
		entropy = crand.Reader
	}

	// Create monotonic reader from entropy source
	reader, err := ulid.NewMonotonicReader(entropy)
	if err != nil {
		return err
	}

	var ulid ulid.ULID

	// Read next ULID from monotonic reader
	if err := reader.Next(now, &ulid); err != nil {
		return err
	}

	// Write ULID to stdout
	str := ulid.String() + "\n"
	stdout.WriteString(str)

	return nil
}

// usage prints usage information and exits with code 0.
func usage() {
	stdout.WriteString("Usage: ulid ...\n" + fflag.Usage())
	unix.Exit(0)
}
