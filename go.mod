module codeberg.org/gruf/go-ulid

go 1.20

require (
	codeberg.org/gruf/go-fflag v0.0.0-20241007195749-357506927a2c
	codeberg.org/gruf/go-fsys v0.0.0-20230508183124-d1809af32b35
	golang.org/x/sys v0.26.0
)

require (
	codeberg.org/gruf/go-bytesize v1.0.3 // indirect
	codeberg.org/gruf/go-byteutil v1.3.0 // indirect
	codeberg.org/gruf/go-split v1.2.0 // indirect
)
