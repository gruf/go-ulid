package ulid_test

import (
	"crypto/rand"
	"errors"
	"os"
	"strconv"
	"testing"

	"codeberg.org/gruf/go-ulid"
)

// iterations is number of test iterations.
var iterations = func() uint64 {
	str := os.Getenv("ITERATIONS")
	iter, _ := strconv.ParseUint(str, 10, 64)
	if iter == 0 {
		// default value
		iter = 10000000
	}
	return iter
}()

func TestMonotonicReaderEntropy(t *testing.T) {
	// Allocate new monotonic reader
	rdr, err := ulid.NewMonotonicReader(rand.Reader)
	if err != nil {
		panic(err)
	}

	// Get timestamp
	ts := ulid.Now()

	var last ulid.ULID

	for i := uint64(0); i < iterations; i++ {
		var u ulid.ULID

		// Generate next ULID
		err := rdr.Next(ts, &u)
		switch err {
		// No issue!
		case nil:

		// This is expected when entropy runs out
		case ulid.ErrMonotonicOverflow:
			return

		// Any other error
		default:
			t.Fatalf("error getting next ULID: %v", err)
		}

		// Ensure this increased
		if cmp := u.Compare(last); cmp <= 0 {
			t.Fatalf("ULID did not increase: cmp=%d last=%q next=%q", cmp, last.String(), u.String())
		}

		// Update last
		last = u
	}
}

func TestMonotonicReaderTimestamp(t *testing.T) {
	// Allocate new monotonic reader
	rdr, err := ulid.NewMonotonicReader(rand.Reader)
	if err != nil {
		panic(err)
	}

	// Get timestamp
	ts := ulid.Now()

	var last ulid.ULID

	for i := uint64(0); i < iterations; i++ {
		var u ulid.ULID

		// Generate next ULID
		err := rdr.Next(ts, &u)
		if err != nil {
			t.Fatalf("error getting next ULID: %v", err)
		}

		// Ensure this increased
		if cmp := u.Compare(last); cmp <= 0 {
			t.Fatalf("ULID did not increase: cmp=%d last=%q next=%q", cmp, last.String(), u.String())
		}

		// Update last
		last = u

		// Incr time
		ts++
	}
}

func TestULIDTimestamp(t *testing.T) {
	// Allocate new monotonic reader
	rdr, err := ulid.NewMonotonicReader(rand.Reader)
	if err != nil {
		panic(err)
	}

	for i := uint64(0); i < iterations; i++ {
		// Get timestamp
		now := ulid.Now()

		var u ulid.ULID

		// Generate next ULID
		err := rdr.Next(now, &u)
		if err != nil {
			t.Fatalf("error getting next ULID: %v", err)
		}

		// Check timestamp
		if u.Timestamp() != now {
			t.Fatal("ULID timestamp not as expected")
		}
	}
}

func TestULIDTextEncoding(t *testing.T) {
	// Base32 ULID encoding
	enc := ulid.Encoding()

	for i := uint64(0); i < iterations; i++ {
		// Generate next ULID
		u, err := ulid.New()
		if err != nil {
			t.Fatalf("error getting next ULID: %v", err)
		}

		// Encode ULID to base32 text
		str := enc.EncodeToString(u[:])

		// Check text encoding
		if str != u.String() {
			t.Fatalf("ULID text encoding not as expected: expect=%q actual=%q", str, u.String())
		}
	}
}

func TestULIDParse(t *testing.T) {
	// Base32 ULID encoding
	enc := ulid.Encoding()

	for i := uint64(0); i < iterations; i++ {
		// Generate next ULID
		u, err := ulid.New()
		if err != nil {
			t.Fatalf("error getting next ULID: %v", err)
		}

		// Encode ULID to base32 text
		str := enc.EncodeToString(u[:])

		// Attempt to parse ULID
		u2, err := ulid.ParseString(str)
		if err != nil {
			t.Fatalf("error parsing ULID: %v", err)
		}

		// Ensure that both ULIDs match
		if u.Compare(u2) != 0 {
			t.Fatalf("parsed ULID does not match: expect=%q actual=%q", u.String(), u2.String())
		}
	}
}

func TestIndeterminateStrings(t *testing.T) {
	type args struct {
		str string
	}

	type want struct {
		errIs error
		idStr string
	}

	tests := []struct {
		name string
		args args
		want want
	}{
		{
			name: "good",
			args: args{
				str: "00000000000000000000000000",
			},
			want: want{
				idStr: "00000000000000000000000000",
			},
		},
		{
			name: "indeterminate",
			args: args{
				str: "04JE0MSNG3B6STYF8H00000011",
			},
			want: want{
				errIs: ulid.ErrInvalidULID,
			},
		},
	}

	for _, testCaseIter := range tests {
		testCase := testCaseIter
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			id, err := ulid.ParseString(testCase.args.str)

			if testCase.want.idStr != "" {
				gotIDStr := id.String()
				if gotIDStr != testCase.want.idStr {
					t.Fatalf("ULID parse from string '%s' produced ULID that serialized to a different value: '%s'", testCase.args.str, gotIDStr)
				}
			}

			if testCase.want.errIs != nil {
				if !errors.Is(err, testCase.want.errIs) {
					t.Fatalf("ULID parse from string '%s' expected error '%v', but got '%v'", testCase.args.str, testCase.want.errIs, err)
				}
			} else {
				if err != nil {
					t.Fatalf("ULID parse from string '%s' expected no error, but got '%v'", testCase.args.str, err)
				}
			}
		})
	}
}

func BenchmarkULIDNew(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			_ = ulid.MustNew()
		}
	})
}

func BenchmarkULIDParse(b *testing.B) {
	u := ulid.MustNew()
	bytes := u.Bytes()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			_, _ = ulid.Parse(bytes)
		}
	})
}

func BenchmarkULIDString(b *testing.B) {
	u := ulid.MustNew()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			_ = u.String()
		}
	})
}
