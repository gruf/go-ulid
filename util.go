package ulid

import (
	"unsafe"
)

// s2b converts string to bytes with no alloc (well, minimal alloc).
func s2b(s string) []byte {
	return unsafe.Slice(unsafe.StringData(s), len(s))
}
